window.addEventListener('load', function() {
   /* for first tab*/
    let button2 = document.getElementsByClassName('your-characteristic-button2');
    let heading2 = document.getElementsByClassName('your-characteristic-heading2');
    let button3 = document.getElementsByClassName('choice-characteristic-main');
    let heading3 = document.getElementsByClassName('choice-characteristic-after-all');
    for (let i = 0; i < button3.length; i++) {
        heading3[0].style.display = 'block';
        heading3[i].style.display = 'none';
        button3[i].addEventListener('click', function () {
            for (let i = 0; i < heading3.length; i++) {
                button3[i].classList.remove('active');
                heading3[i].style.display = 'none'
            }

            this.classList.add('active');
            heading3[i].style.display = 'block'
        });
    }
    for (let i = 0; i < button2.length; i++) {
        button2[i].addEventListener('click', function () {
            for (let u = 0; u < heading2.length; u++) {
                heading2[u].style.display = 'none';
                button2[u].classList.remove('active');
            }

            heading2[i].style.display = 'block';
            this.classList.add('active');
        });
    }




    /* for second tab*/
    let button = document.getElementsByClassName('your-characteristic-button');
    let heading = document.getElementsByClassName('your-characteristic-heading');
    let buttonCar = document.getElementsByClassName('fit-your-choice-car');
    let headingCar = document.getElementsByClassName('your-characteristic');
    for (let i = 0; i < button.length; i++) {
        button[i].addEventListener('click', function () {
            for (let u = 0; u < heading.length; u++) {
                heading[u].style.display = 'none';
                button[u].classList.remove('active');
            }

            heading[i].style.display = 'block';
            this.classList.add('active');
        });
    }
    for (let i = 0; i < buttonCar.length; i++) {
        buttonCar[i].onclick = function () {
            heading[i * 4].style.display = 'block';
            button[i * 4].classList.add('active');
            myFunction()
        };
        headingCar[i].style.display = 'none';

        function myFunction() {
            if (headingCar[i].style.display === 'none') {
                for (let i = 0; i < headingCar.length; i++) {
                    headingCar[i].style.display = 'none';
                }
                headingCar[i].style.display = 'block';
            } else {
                headingCar[i].style.display = 'none';
                for (let i = 0; i < headingCar.length; i++) {
                    headingCar[i].style.display = 'none';
                }
            }
        }
    }

     /* slider for car characteristics */
     $('#characteristic-slider').slick({
        infinite: true,
        slidesToShow: 3, // Shows a three slides at a time
        slidesToScroll: 1,
        arrows: true,
        dots:false,
        responsive: [
            {
                breakpoint: 1240,
                settings: {
                    slidesToShow: 2,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                },
            }
        ]
    });





});